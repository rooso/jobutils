#! /bin/bash

cd /Users/oroos/Post-stage/SIMUS/LOGO

if [ ! -e "/Users/oroos/Post-stage/SIMUS/LOGO/mass_loading" ] ; then
    touch mass_loading #if does not exist, create it
else
    gtruncate -s 0 mass_loading #erase content if it exists
fi

for i in L1 L2_KThR+AGN LL3 ; do 
	cd $i 
	last=$(ls part_*.dat | tail -n 1) 
	lastsfr=$(ls part_*.dat.sfr | tail -n 1)
	if [[ "$lastsfr" != "$last"* ]] ; then ipython --matplotlib -c "%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=10 -out" ; fi
	last=$(ls part_*.dat.sfr | tail -n 1) 
	lastn=${last//.dat.sfr} 
	lastn=${lastn//part_00}
	cd ../L$i
	last=$(ls part_*.dat | tail -n 1)     
        lastsfr=$(ls part_*.dat.sfr | tail -n 1)
        if [[ "$lastsfr" != "$last"* ]] ; then ipython --matplotlib -c "%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=10 -out" ; fi
	last=$(ls part_*.dat.sfr | tail -n 1)
	lastn_lores=${last//.dat.sfr}
        lastn_lores=${lastn_lores//part_00}
        cd -
	ipython --matplotlib -c "%run ~/python/Post-stage/outflows.py --proceeding --all --max --sfr $lastn --lores --sfr_lores $lastn_lores" >> ../mass_loading 
	cd .. 
done

for i in L1_KThR L1_AGN L2_KThR L2_AGN ; do 
	cd $i 
	last=$(ls part_*.dat | tail -n 1)     
	lastsfr=$(ls part_*.dat.sfr | tail -n 1)
        if [[ "$lastsfr" != "$last"* ]] ; then ipython --matplotlib -c "%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=10 -out" ; fi
	last=$(ls part_*.dat.sfr | tail -n 1) 
	lastn=${last//.dat.sfr} 
	lastn=${lastn//part_00}
	cd ../L$i
	last=$(ls part_*.dat | tail -n 1)     
	lastsfr=$(ls part_*.dat.sfr | tail -n 1)
        if [[ "$lastsfr" != "$last"* ]] ; then ipython --matplotlib -c "%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=10 -out" ; fi
	last=$(ls part_*.dat.sfr | tail -n 1)
        lastn_lores=${last//.dat.sfr}
        lastn_lores=${lastn_lores//part_00}
        cd -
	ipython --matplotlib -c "%run ~/python/Post-stage/outflows.py --mix --proceeding --max --sfr $lastn --lores --sfr_lores $lastn_lores" >> ../mass_loading 
	cd .. 
done

for i in LLL3_KThR LLL3_AGN ; do 
        cd $i 
        last=$(ls part_*.dat | tail -n 1)     
	lastsfr=$(ls part_*.dat.sfr | tail -n 1)
        if [[ "$lastsfr" != "$last"* ]] ; then ipython --matplotlib -c "%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=10 -out" ; fi
	last=$(ls part_*.dat.sfr | tail -n 1) 
        lastn=${last//.dat.sfr} 
        lastn=${lastn//part_00}
	echo $LOGO/$i #remplace affichage de 'cd -' des autres boucles 
        ipython --matplotlib -c "%run ~/python/Post-stage/outflows.py --mix --proceeding --max --sfr $lastn" >> ../mass_loading
        cd .. 
done

open L1/outflow_rate_L1_all_max_lores.pdf L2_KThR+AGN/outflow_rate_L2_KThR+AGN_all_max_lores.pdf LL3/outflow_rate_LL3_all_max_lores.pdf

ipython --matplotlib -c "%run /Users/oroos/python/Post-stage/mass_loadings.py"
open mass-loadings_vs_mass.pdf
