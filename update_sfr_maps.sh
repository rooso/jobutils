#! /bin/bash

cd /Users/oroos/Post-stage/SIMUS/LOGO

if [[ "$1" == "DL" ]]; then
for i in `ls -d L*/` ; do echo $i ; scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_lores/$i/output/*.gz ./$i ; done
fi 

for i in `ls -d L*/` ; do 
	echo $i
	cd $i
	maps=$(ls map*T.fits.gz)
	maps=${maps//map_00}
	maps=${maps//_T.fits.gz} 
	for m in $maps ; do
		ipython --matplotlib -c "%run ~/python/Post-stage/sfr_map.py 00$m --large --disk" 
		ipython --matplotlib -c "%run ~/python/Post-stage/sfr_map.py 00$m --disk"
		ipython --matplotlib -c "%run ~/python/Post-stage/sfr_map.py 00$m --large"
		ipython --matplotlib -c "%run ~/python/Post-stage/sfr_map.py 00$m"
	done
	cd .. 
done


