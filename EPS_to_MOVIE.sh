#! /bin/bash

if [[ "$1" == "fits_all" ]]; then
	files=fits_00*all.eps
	movie_name=fits_all
fi
if [[ "$1" == "fits_all_large" ]]; then
	files=fits_00*all_large.eps
	movie_name=fits_all_large
fi
if [[ "$1" == "outflows_nTzv_out" ]]; then
	files=outflows_nTzv_out_00*[0-9].eps
	movie_name=outflows_nTzv_out
fi
if [[ "$1" == "sfr_map" ]]; then
	files=sfr_map_00*large.eps
	movie_name=sfr_map_large
fi
if [[ "$1" == "fits_P_large" ]]; then
	files=fits*P_large_no-vel.eps
	movie_name=fits_P_large
fi

if [ ! -e MOVIES ] ; then mkdir MOVIES ; fi

#ls $files

for i in `ls $files` ; do 
	if [ -f MOVIES/${i//eps}jpg ] ; then
		echo MOVIES/${i//eps}jpg exists...
	else
		echo convert $i ${i//eps}jpg 
		convert $i ${i//eps}jpg 
		mv ${i//eps}jpg MOVIES  
	fi
done

fps=6

cd ./MOVIES

if [[ "$1" == "fits_P_large" ]]; then
	/opt/local/bin/ffmpeg -y -r $fps -f image2 -pattern_type glob -i 'fits*P_large_no-vel.jpg' $movie_name.mpg
fi
if [[ "$1" == "sfr_map" ]]; then
	/opt/local/bin/ffmpeg -y -r $fps -f image2 -pattern_type glob -i 'sfr_map*large.jpg' $movie_name.mp4
fi
if [[ "$1" == "outflows_nTzv_out" ]]; then	
	/opt/local/bin/ffmpeg -y -r $fps -f image2 -pattern_type glob -i 'outflows_nTzv_out_00*.jpg' -vf 'scale=720:-2' $movie_name.mp4
fi
if [[ "$1" == "fits_all_large" ]]; then
	/opt/local/bin/ffmpeg -y -r $fps -f image2 -pattern_type glob -i 'fits_00*all_large.jpg' -vf 'scale=720:-2' $movie_name.mp4
fi
if [[ "$1" == "fits_all" ]]; then
	/opt/local/bin/ffmpeg -y -r $fps -f image2 -pattern_type glob -i 'fits_00*all.jpg' -vf 'scale=720:-2' $movie_name.mp4
fi


###-vf 'scale=720:-2'
