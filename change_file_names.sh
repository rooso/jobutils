#! /bin/bash

#pour tous les fichiers density_profile* (en ignorant les fichiers contenant 'ignore', faire :
for i in $( ls heating_rate* | grep -v '*2.eps' ); do

#renommer le fichier en remplacant density par quasar_density
mv $i ${i/heating_rate/rel_temp_change};

done

##pour tous les fichiers files_* (en ingorant les fichiers contenant 'ignore', faire :
#for i in $( ls files_* | grep -v 'ignore' ); do

##renommer le fichier en supprimant remove du nom original
#mv $i ${i%remove};

#done
