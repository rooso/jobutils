#! /bin/bash
dir=$(pwd)
dir=$(echo $dir | tr '/' '\n')
dir=$(echo $dir | awk '{print $6}')
echo $dir


if [[ $# -lt 1 ]] ; then  ###If wrong number of args, print help
    echo "Usage :"
    echo "This script will open movies, launch all interesting python scripts and generate .eps files of specified POGO simulation output."
    echo ""
    echo "$1 : output number (3 digits)"
    echo "$2 [optional] : if need to dowload files from Curie, set to DL"
    echo "$3 [optional] : if DL only, set to o"
    echo ""
    echo "Example : Lets_POGO.sh 098 [DL o]"
    exit 1
fi

if [[ "$2" == "DL" ]]; then ###Download from curie ?? 
	if [[ "$dir" == "M"* ]] ; then ddir="Project" ; fi
	if [[ "$dir" == "L"* ]] ; then ddir="lores" ; fi
	echo $1
	scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/outflow_vs_time_dat.txt . 
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/*$1*.fits.gz .
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/MOVIE/*.mp4 .
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/*$1.dat .
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/outflows_nTzv_out_00$1*.eps .
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/outflows_nTzv_out_00$1*.pysav .
        scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/statistics_00$1*_outflow.ascii .
fi

if [[ "$2" == "HB" ]]; then ###Download from HUBBLE
        echo $1
	echo "Downloading from Curie to Hubble..."
	ssh -Y oroos@hubble.extra.cea.fr "cd $dir ; /home/oroos/Lets_POGO_HB.sh $1 DL o ; echo 'Done' "
	echo "Now downloading from Hubble to Mac..."
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/*.txt .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/*$1*.fits.gz .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/*.mp4 .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/*$1.dat .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/outflows_nTzv_out_00$1*.eps .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/outflows_nTzv_out_00$1*.pysav .
        scp -r oroos@hubble.extra.cea.fr:/home/oroos/$dir/statistics_00$1*_outflow.ascii .
fi


if [[ "$3" == "o" ]]; then ###Only download files
	exit
fi

###Managing subdivisions of pysav files
name=$(ls outflows_nTzv_out_00$1*.pysav) ###find file name
test=${name//.pysav}                  
test=${test//outflows_nTzv_out_00$1}
test=$(echo $test | awk '{print $1}')    ###check if parts exist
if [[ "$test" != "" ]] ; then
	test=$(echo '--test '$test)      ###enable option
fi
echo $test

###/!\ SFR computation : needs value of f_w BUT set for all time...
fw=10 ### NOT A PROBLEM !! (from Florent) : change only if eta_sn changes.
if [[ "$dir" == *"_AGN" ]] ; then
	fw=0
	echo "f_w set to 0 bec. of residual stellar FB !!!"
fi
###SFR computation : only want latest curve, even for older outputs
last=$(ls part_*.dat | tail -n 1)
lastn=${last//.dat}
lastn=${lastn//part_00}

sfr=""
out="-out"
all=""
if [[ "$dir" == *"_KThR+AGN" ]] || [[ "$dir" == *"1" ]] || [[ "$dir" == *"3" ]] ; then
	sfr="--sfr $lastn"
	if [[ "$dir" != *"3" ]] ; then all="--all" ; fi
fi
###echo $dir $sfr $out $all

#Open movies and launch all Python routines
echo "POGO is starting..."
interactive="-i"
yes=""
yess=""
if [[ "$2" == "ni" || "$3" == "ni" ]]; then ###Shut off interactivity
        interactive=""
else
	open *.mp4 
	#open outflows_nTzv_out_00$1.eps
	yes="%run ~/python/Post-stage/plot_sfr.py $last -eta_sn=0.2 -f_w=$fw $out"
	yess="%run ~/python/Post-stage/outflows.py $sfr $all"
fi
/Users/oroos/anaconda/bin/ipython --matplotlib $interactive -c "$yes
$yess
%run ~/python/Post-stage/show_fits.py 00$1 -t all 
%run ~/python/Post-stage/show_fits.py 00$1 -t all --large
%run ~/python/Post-stage/show_fits.py 00$1 -t all --disk 
%run ~/python/Post-stage/sfr_map.py 00$1  
%run ~/python/Post-stage/sfr_map.py 00$1 --large --silent
%run ~/python/Post-stage/sfr_map.py 00$1 --disk --silent 
%run ~/python/Post-stage/rho_T_diagram_sequential_no-units.py -o 00$1 $test
"
 

#Alternate commands :
##%run ~/python/Post-stage/rho_T_diagram_sequential_no-units.py -o 00$1 
##%run ~/python/Post-stage/show_fits.py all [-t ...]
##%run ~/python/Post-stage/show_fits.py 00$1 -t [any type : v_*|level|rho|T|...]
##%run ~/python/Post-stage/rho_T_diagram.py -o 00$1 --Jared
##%run ~/python/Post-stage/rho_T_diagram.py -o 00$1 -v nT
