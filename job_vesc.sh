#! /bin/bash
#following job_vesc on Curie, run this code.

dir=$(pwd)
dir=$(echo $dir | tr '/' '\n')
dir=$(echo $dir | awk '{print $6}')
echo $dir

if [[ "$dir" == "M"* ]] ; then ddir="Project" ; fi
if [[ "$dir" == "L"* ]] ; then ddir="lores" ; fi

scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/gas_part_00*_vesc_lmax13.ascii .
scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_$ddir/$dir/output/VESC_00*_lmax13.DAT .
    
for i in `ls VESC_00*_lmax13.DAT` ; do
    i=${i//_lmax13.DAT}
    i=${i//VESC_00}
    echo $i
    cp VESC_00${i}_lmax13.DAT VESC.DAT
    cp gas_part_00${i}_vesc_lmax13.ascii input.dat
    /Users/oroos/anaconda/bin/ipython --matplotlib -c "%run ~/python/Post-stage/Vescape.py"
    mv vesc.eps vesc_00${i}_lmax13.eps
done

open vesc*.eps
