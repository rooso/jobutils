for i in `ls map_*_T_large.fits.gz` ; do
	i=${i//_T*}
	i=${i//map_}
	/Users/oroos/anaconda/bin/ipython --matplotlib -c "%run ~/python/Post-stage/show_fits.py $i -t T --large --vel-factor=4 --vel-skip=100"
done
