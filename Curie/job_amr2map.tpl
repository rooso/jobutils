#!/bin/bash
#MSUB -A gen2192 ###ra2552
### name of the job
#MSUB -r amr2map_%(name)s
### Job logfile and error file.  %%J is the job #
#MSUB -o logfile_%%J.o
#MSUB -e logfile_%%J.e
### number of CPUs to use
#MSUB -n 1
### amount of memory to use per processor in MB
#MSUB -M 4000
### Time (in seconds)
#MSUB -T 86400
### use thin nodes
#MSUB -q standard ###xlarge

set -x

cd $BRIDGE_MSUB_PWD

for j in 0 1 2 3 4 7 ; do
    echo 'amr2map -inp %(nout)s -dir y -ymin 0.49 -ymax 0.51 -xmin 0.475 -xmax 0.525 -zmin 0.475 -zmax 0.525 -typ $j -lmax 17'
    amr2map -inp %(nout)s -dir y -ymin 0.49 -ymax 0.51 -xmin 0.475 -xmax 0.525 -zmin 0.475 -zmax 0.525 -typ $j -lmax 17

    echo 'amr2map -inp %(nout)s -dir z -xmin 0.475 -xmax 0.525 -ymin 0.475 -ymax 0.525 -zmin 0.495 -zmax 0.505 -typ $j -lmax 17 -out disk'
    amr2map -inp %(nout)s -dir z -xmin 0.475 -xmax 0.525 -ymin 0.475 -ymax 0.525 -zmin 0.49 -zmax 0.51 -typ $j -lmax 17 -out 'disk'

    echo amr2map -inp %(nout)s -dir y -ymin 0.49 -ymax 0.51 -xmin 0.35 -xmax 0.65 -zmin 0.35 -zmax 0.65 -typ $j -lmax 14 -out 'large'
    amr2map -inp %(nout)s -dir y -ymin 0.49 -ymax 0.51 -xmin 0.35 -xmax 0.65 -zmin 0.35 -zmax 0.65 -typ $j -lmax 14 -out 'large'

done

