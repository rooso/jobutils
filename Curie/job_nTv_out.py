#!/usr/bin/env python

from commands import getstatusoutput as cmd
from argparse import ArgumentParser
import os 

dir = os.getcwd()

#argument and option parser
parser = ArgumentParser()
parser.add_argument("outpt", type=str, #nargs='+'
                  help="output number", metavar="OUTPUT")
parser.add_argument("-lmax", dest="lmax", default=15, type=int,
                  help="maximum level of refinement for map")
args = parser.parse_args()
outpt = args.outpt #outpt = 'output_'+args.outpt
lmax = args.lmax
n = outpt[-5:]

tpl = open('/ccc/scratch/cont003/dsm/rooso/POGO_Project/job_utils/job_nTv_out.tpl','r').read()
data = {'nout': outpt, 'lm': lmax, 'name': (dir.split('/'))[-2], 'n':n}
 
with open('job_nTv_out_'+outpt,'w') as job:
    job.write(tpl % data)

st, out = cmd('ccc_msub job_nTv_out_'+outpt)
print out
if st != 0:
    print st


