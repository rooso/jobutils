#!/usr/bin/env python

from commands import getstatusoutput as cmd
from argparse import ArgumentParser
import os

dir = os.getcwd()

#argument and option parser
parser = ArgumentParser()
parser.add_argument("outpt", type=str, #nargs='+'
                  help="output number", metavar="OUTPUT")
args = parser.parse_args()
outpt = args.outpt #outpt = 'output_'+args.outpt

tpl = open('/ccc/scratch/cont003/dsm/rooso/POGO_Project/job_utils/job_amr2map.tpl','r').read()
data = {'nout': outpt, 'name': (dir.split('/'))[-2]}
 
with open('job_amr2map_'+outpt,'w') as job:
    job.write(tpl % data)

st, out = cmd('ccc_msub job_amr2map_'+outpt)
print out
if st != 0:
    print st


