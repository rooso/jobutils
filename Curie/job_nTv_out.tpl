#!/bin/bash
#MSUB -A gen2192 ###ra2552
### name of the job
#MSUB -r nTv_out_%(name)s
### Job logfile and error file.  %%J is the job #
#MSUB -o logfile_%%J.o
#MSUB -e logfile_%%J.e
### number of CPUs to use
#MSUB -n 1
### amount of memory to use per processor in MB
#MSUB -M 4000
### Time (in seconds)
#MSUB -T 86400
### use thin nodes
#MSUB -q standard ###xlarge

set -x

cd $BRIDGE_MSUB_PWD

amr2map -inp %(nout)s -outflow -dir y -xmin 0.35 -xmax 0.65 -ymin 0.45 -ymax 0.55 -zmin 0.35 -zmax 0.65 -lmax %(lm)s

###nlines=`cat gas_part_%(n)s_outflow.ascii | wc -l`
nlines=$(wc -l gas_part_%(n)s_outflow.ascii | awk '{print $1}')
if [[ $nlines -gt 200000000 ]] ; then
  echo 'Splitting file...'
  (( half = $nlines/2 ))
  head -n $half gas_part_%(n)s_outflow.ascii > gas_part_%(n)s_part1_outflow.ascii
  (( rem = $nlines-$half ))
  tail -n $rem gas_part_%(n)s_outflow.ascii > gas_part_%(n)s_part2_outflow.ascii
  stat=$(more statistics_%(n)s_outflow.ascii | awk '{print $2 " " $3}')
  echo $half $stat > statistics_%(n)s_part1_outflow.ascii
  echo $rem  $stat > statistics_%(n)s_part2_outflow.ascii

  ~/bin/job_nTv_out_eps.py %(n)s -t '_part1'
  ~/bin/job_nTv_out_eps.py %(n)s -t '_part2'
##### if needed, divide in more than 2 files
else
  ~/bin/job_nTv_out_eps.py %(n)s
fi


