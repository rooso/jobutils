#!/bin/bash
#MSUB -A gen2192 ###ra2552
### name of the job
#MSUB -r eps_%(name)s+%(test)s
### Job logfile and error file.  %%J is the job #
#MSUB -o logfile_%%J.o
#MSUB -e logfile_%%J.e
### number of CPUs to use
#MSUB -n 1
### amount of memory to use per processor in MB
#MSUB -M 4000
### Time (in seconds)
#MSUB -T 86400
### use thin nodes
#MSUB -q standard ###xlarge

set -x

cd $BRIDGE_MSUB_PWD

~/bin/rho_T_diagram_sequential_no-units.py -o %(nout)s %(test)s
