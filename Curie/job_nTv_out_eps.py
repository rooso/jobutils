#!/usr/bin/env python

from commands import getstatusoutput as cmd
from argparse import ArgumentParser
import os 

dir = os.getcwd()

#argument and option parser
parser = ArgumentParser()
parser.add_argument("outpt", type=str, #nargs='+'
                  help="output number", metavar="OUTPUT")
parser.add_argument("-a", dest="job_id", type=str,
                  help="job-id of the job generating gas_part*.ascii")
parser.add_argument("-t", dest="test", type=str, default='',
                  help="suffix for file name")
args = parser.parse_args()
outpt = args.outpt #outpt = 'output_'+args.outpt
job_id = args.job_id
if args.test == '':
    ttttt = ''
else:
    ttttt = '--test '+args.test
nout = outpt[-5:]

tpl = open('/ccc/scratch/cont003/dsm/rooso/POGO_Project/job_utils/job_nTv_out_eps.tpl','r').read()
data = {'nout': nout, 'name': (dir.split('/'))[-2], 'test':ttttt}
 
with open('job_nTv_out_eps_'+outpt+args.test,'w') as job:
    job.write(tpl % data)

if (job_id is None):
    job_cmd = 'ccc_msub job_nTv_out_eps_'+outpt+args.test
else:
    job_cmd = 'ccc_msub -a '+job_id+' job_nTv_out_eps_'+outpt+args.test

st, out = cmd(job_cmd)
print out
if st != 0:
    print st


