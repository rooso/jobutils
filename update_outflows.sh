#! /bin/bash

cd /Users/oroos/Post-stage/SIMUS/LOGO
for i in `ls -d L*/` ; do echo $i ; scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_lores/$i/output/*.txt ./$i ; done
for i in `ls -d L[12]*/ LL3/` ; do cd $i ; ipython --matplotlib -c "%run ~/python/Post-stage/outflows.py --lores --proceeding" ; cd .. ; done
for i in L1 L2_KThR+AGN LL3; do 
	cd $i 
	last=$(ls part_*.dat.sfr | tail -n 1) 
	lastn=${last//.dat.sfr} 
	lastn=${lastn//part_00}
	cd ../L$i 
	last=$(ls part_*.dat.sfr | tail -n 1) 
	lastn_lores=${last//.dat.sfr} 
	lastn_lores=${lastn_lores//part_00} 
	cd - 
	ipython --matplotlib -c "%run ~/python/Post-stage/outflows.py --all --sfr $lastn --lores --sfr_lores $lastn_lores --proceeding" 
	cd .. 
done

ls L*/outflow_rate*_lores.pdf
open L*/outflow_rate*_lores.pdf

#ls L*/outflow_rate*[123RN]_lores.eps
#open L*/outflow_rate*[123RN]_lores.eps
