#! /bin/bash

dir=$(pwd)
dir=$(echo $dir | tr "/" "\n")
dir=$(echo $dir | awk '{print $6}')
echo $dir

echo Scanning files...
files=$(ssh rooso@curie-ccrt.ccc.cea.fr "pwd ; cd /ccc/scratch/cont003/dsm/rooso/POGO_*/$dir/output ; ls -1 map_00*.fits.gz outflow*.pysav part*.dat stat*.asciimovie*mp4 | grep -v side | grep -v disk_large | grep -v vesc | grep -v test")
files=${files//\/ccc\/cont003\/dsku\/zinc\/home\/user\/rooso}
files_here=$(ls -1 map_00*.fits.gz outflow*.pysav part*.dat stat*.ascii movie*mp4 | grep -v side | grep -v disk_large | grep -v vesc | grep -v test)
for i in $files_here ; do 
	echo $i 
	diff --brief $i <(ssh rooso@curie-ccrt.ccc.cea.fr "cat /ccc/scratch/cont003/dsm/rooso/POGO_*/$dir/output/$i")
	if [[ "$?" == "0" ]] ; then ###if files are identical, then do not copy
		files=${files//$i} 
	fi
done

echo "Remaining files to DL :"
files=${files//\/ccc\/cont003\/dsku\/zinc\/home\/user\/dsm\/rooso} #prevent annoying bug coming from Chuck Norris knows where
echo $files
echo 'Downloading files...'
for i in $files ; do
	#echo 'Downloading file...' $i
	scp -r rooso@curie-ccrt.ccc.cea.fr:/ccc/scratch/cont003/dsm/rooso/POGO_*/$dir/output/$i .
	#keep output number
	temp=${i#*00} ### remove shortest string before 00, including 00
	temp=${temp%%[_.]*} ### remove longest string after _ or ., including _ or .
	#echo $temp
	pogo+=($temp) #create new array and append number
	#echo ${pogo[@]}
done
pogo=$(echo ${pogo[@]})

#Remove duplicates ; keep only 1 occurence
#pogo=$(awk '!pogo[$0]++')
pogo=$(tr ' ' '\n' <<< "${pogo[@]}" | sort -u | tr '\n' ' ')

echo Launching Lets_pogo.sh on : $pogo
for i in $pogo ; do
	~/Scripts_divers/Lets_pogo.sh $i ni
done

files=$(ls map*T.fits.gz)
files=${files//_T.fits.gz}
files=${files//map_00}
for i in $files ; do
	n=$(ls *$i*.eps | wc -l) 
	if (( "$n" < 7 )) ; then 
		if [[ "$pogo" != *"$i"* ]] ; then
			n=$(ls *$i* | grep -v eps | wc -l) 
			if (( "$n" < 21 )) ; then 
				echo 'Data files missing for ' $i '!!!'
				touch Data_files_missing_for_$i 
			else
				echo $i 'Missing eps files... Re-launching Lets_pogo'
				~/Scripts_divers/Lets_pogo.sh $i ni
			fi
		fi
	fi 
done
